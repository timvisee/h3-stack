[![Pipeline status](https://gitlab.com/timvisee/h3-stack/badges/master/pipeline.svg)](https://gitlab.com/timvisee/h3-stack/commits/master)

# Hospital Haagse Hogeschool
This is the server configuration repository of the HHH stack.

For this service stack, we're using Docker for which each service has their own
Docker image.

## Usage
To start the stack based on the Docker compose configuration, use:

```bash
docker-compose up
# or
docker-compose up -d
```

Note that it might take up to a few minutes on first start up before the
PowerDNS database is initialized. PowerDNS will be reporting database
connectivity errors during that period. This process is done when
`MySQL init process done. Ready for start up.` is shown.
Subsequent startups should be ready in seconds.

## Installation
Some additional steps (besides running the Docker stack) are required to get
this to work:

1. Add interface configuration to host:  
   Add [`./dhcp/interfaces`](./dhcp/interfaces) to `/etc/network/interfaces`.  
   Renaming `eno1` to the host native interface may be required, also update the
   name in [`./dhcp/isc-dhcp-server`](./dhcp/isc-dhcp-server) and rebuild/pull
   the updated images.

Finally reboot the machine to ensure every configuration is applied.

## Builds
Docker images for our services are automatically build through GitLab CI, on
each commit.
