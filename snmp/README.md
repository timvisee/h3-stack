# SNMPD

> TODO: use static snmpd.conf file in image, switch to Docker compose

## Quick and Dirty

Launch listening on "public" like this:
```
$ docker run -d -v /proc:/host_proc \
    --privileged \
    --read-only \
    -p 161:161/udp \
    --name snmpd
    really/snmpd
```

## Your own snmpd.conf
```
$ docker run -d -v /my/snmpd.conf:/etc/snmp/snmpd.conf \
    -v /proc:/host_proc \
    --privileged \
    --read-only \
    -p 161:161/udp \
    --name snmpd
    really/snmpd
```
