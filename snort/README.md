# Snort

> TODO: use static configuration in image, switch to Docker compose

Attach the snort in container to have full access to the network.
```
$ docker run -it --rm --net=host linton/docker-snort /bin/bash
```

Or you may need to add `--cap-add=NET_ADMIN` or `--privileged` (unsafe.)
```
$ docker run -it --rm --net=host --cap-add=NET_ADMIN linton/docker-snort /bin/bash
```
